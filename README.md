# check-version


```
feat(hat): JIRA-2313 add wobble :tophat:
.--- .--   .-------- .--------- .-------
|    |     |         |          |
|    |     |         |          '-> emoji
|    |     |         |      
|    |     |         '--> summary in present tense
|    |     |
|    |     '-----> jira ticket id
|    |
|    '-------> scope of the changeset
|
'--------> type: chore, docs, feat, fix, refactor, style, or test
```

### ENGLISH VERSION

build 	    Builds 	                    Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm) 	        🛠
chore 	    Chores 	                    Other changes that don't modify src or test files 	                                                            ♻️
ci 	        Continuous Integrations 	Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs) 	⚙️
docs 	    Documentation 	            Documentation only changes 	                                                                                    📚
feat 	    Features 	                A new feature 	                                                                                                ✨
fix 	    Bug Fixes 	                A bug Fix 	                                                                                                    🐛
perf 	    Performance Improvements    A code change that improves performance 	                                                                    🚀
refactor    Code Refactoring 	        A code change that neither fixes a bug nor adds a feature 	                                                    📦
revert 	    Reverts 	                Reverts a previous commit                                                                                       🗑
style 	    Styles 	                    Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) 	        💎
test 	    Tests 	                    Adding missing tests or correcting existing tests 	                                                            🚨


### FRENCH VERSION

build : changements qui affectent le système de build ou des dépendances externes (npm, make…)
ci : changements concernant les fichiers et scripts d’intégration ou de configuration (Travis, Ansible, BrowserStack…)
docs : rédaction ou mise à jour de documentation
feat : ajout d’une nouvelle fonctionnalité
fix : correction d’un bug
perf : amélioration des performances
refactor : modification qui n’apporte ni nouvelle fonctionalité ni d’amélioration de performances
revert: Ce dernier permet comme son nom l’indique, d’annuler un précédent commit
style : changement qui n’apporte aucune alteration fonctionnelle ou sémantique (indentation, mise en forme, ajout d’espace, renommante d’une variable…)
test : ajout ou modification de tests
